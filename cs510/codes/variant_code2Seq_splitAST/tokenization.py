import pandas as pd
import pickle
import re
import numpy as np
import javalang
from extractor import Extractor
from common import Common
from model import Model
from config import Config
debugFile = open('logFile.log','w+')

SHOW_TOP_CONTEXTS = 10
MAX_PATH_LENGTH = 8
MAX_PATH_WIDTH = 2
EXTRACTION_API = 'https://po3g2dx2qa.execute-api.us-east-1.amazonaws.com/production/extractmethods'
code2SeqModelFile = '../code2seq_pretrainedModel/model_iter52.release'

# Load the data:
with open('../../data/raw/train.pickle', 'rb') as handle:
    train = pickle.load(handle)
with open('../../data/raw/valid.pickle', 'rb') as handle:
    valid = pickle.load(handle)
with open('../../data/raw/test.pickle', 'rb') as handle:
    test = pickle.load(handle)

config = Config.get_default_config(code2SeqModelFile)
model = Model(config)


# Tokenize and shape our input:
def custom_tokenize(string):
    try:
        tokens = list(javalang.tokenizer.tokenize(string))
    except:
        return []
    values = []
    for token in tokens:
        # Abstract strings
        if '"' in token.value or "'" in token.value:
            values.append('$STRING$')
        # Abstract numbers (except 0 and 1)
        elif token.value.isdigit() and int(token.value) > 1:
            values.append('$NUMBER$')
        #other wise: get the value
        else:
            values.append(token.value)
    #print(values)
    return values


def tokenize_df(df):
    df['instance'] = df['instance'].apply(lambda x: custom_tokenize(x))
    df['context_before'] = df['context_before'].apply(lambda x: custom_tokenize(x))
    df['context_after'] = df['context_after'].apply(lambda x: custom_tokenize(x))
    return df


def tokenizeUsingcode2seq_df(df,reachSkipNum):
    code2SeqDict = {
        'start_token' : [] ,
        'end_token' : [],
        'ast_path' : [],
        'is_buggy' : []
    }


    reachCount=0
    skipCount=0

    for idx, row in df.iterrows():
        print(str(idx))
        input_context = row["context_before"] +row["instance"] +row["context_after"]
        path_extractor = Extractor(config, EXTRACTION_API, MAX_PATH_LENGTH, max_path_width=2)
        predict_lines, pc_info_dict = path_extractor.extract_paths(input_context)
        if(len(pc_info_dict.items())==0):
            skipCount+=1
            continue
        model_results = model.predict(predict_lines)
        prediction_results = Common.parse_results(model_results, pc_info_dict, topk=SHOW_TOP_CONTEXTS)
        for index, method_prediction in prediction_results.items():
            for timestep, single_timestep_prediction in enumerate(method_prediction.predictions):
               for attention_obj in single_timestep_prediction.attention_paths:
                   code2SeqDict['start_token'].append(attention_obj['token1'])
                   code2SeqDict['end_token'].append(attention_obj['token2'])
                   code2SeqDict['ast_path'].append(attention_obj['path'])
                   code2SeqDict['is_buggy'].append(row['is_buggy'])
               break
            break
        reachCount+=1
        if(reachSkipNum < reachCount):
            break

    #print(code2SeqDict)
    tokenizedDf = pd.DataFrame.from_dict(code2SeqDict)
    return tokenizedDf,skipCount,reachCount


test,skip,reach = tokenizeUsingcode2seq_df(test,3)
print("\nCases Skipped"+str(skip)+"\nCases tokenized : "+str(reach))

with open('../../data/tokenized_test.pickle', 'wb') as handle:
    pickle.dump(test, handle, protocol=pickle.HIGHEST_PROTOCOL)


valid,skip,reach = tokenizeUsingcode2seq_df(valid,3)
print("\nCases Skipped"+str(skip)+"\nCases tokenized : "+str(reach))

with open('../../data/tokenized_valid.pickle', 'wb') as handle:
    pickle.dump(valid, handle, protocol=pickle.HIGHEST_PROTOCOL)


train,skip,reach = tokenizeUsingcode2seq_df(train,10)
print("\nCases Skipped"+str(skip)+"\nCases tokenized : "+str(reach))

with open('../../data/tokenized_train.pickle', 'wb') as handle:
    pickle.dump(train, handle, protocol=pickle.HIGHEST_PROTOCOL)
