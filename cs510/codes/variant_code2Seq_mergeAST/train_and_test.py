import pickle
import pandas as pd
import os
import math
import numpy as np
import tensorflow_datasets as tfds
import tensorflow as tf
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.callbacks import ModelCheckpoint
from tensorflow.keras.utils import Sequence
from sklearn.metrics import roc_curve, auc
import matplotlib.pyplot as plt

import sys

class Logger(object):
    def __init__(self):
        self.terminal = sys.stdout
        self.logFileName = os.getcwd()+"train_test_stdout.log"
        if(os.path.exists(self.logFileName)):
            os.remove(self.logFileName)
        self.log = open(self.logFileName, "w")

    def write(self, message):
        self.terminal.write(message)
        self.log.write(message)

    def flush(self):
        #this flush method is needed for python 3 compatibility.
        #this handles the flush command by doing nothing.
        #you might want to specify some extra behavior here.
        pass

sys.stdout = Logger()

maxlen = 1500

# Loading tokenized data

with open('../../data/tokenized_code2seq/tokenized_train.pickle', 'rb') as handle:
    train = pickle.load(handle)
with open('../../data/tokenized_code2seq/tokenized_valid.pickle', 'rb') as handle:
   valid = pickle.load(handle)
with open('../../data/tokenized_code2seq/tokenized_test.pickle', 'rb') as handle:
   test = pickle.load(handle)

def reshape_instances(df):
    X_df = []
    Y_df = []

    for index, rows in df.iterrows():
        if(rows['input_tokens'] == ""):
            continue
        X_df.append(rows['input_tokens'] + rows['input_path'])
        Y_df.append(rows.is_buggy)
    return X_df, Y_df


'''

with open('../../data/tokenized_javalang/tokenized_train.pickle', 'rb') as handle:
    train = pickle.load(handle)
with open('../../data/tokenized_javalang/tokenized_valid.pickle', 'rb') as handle:
   valid = pickle.load(handle)
with open('../../data/tokenized_javalang/tokenized_test.pickle', 'rb') as handle:
   test = pickle.load(handle)

# Reshape instances:
def reshape_instances(df):
    df["input"] = df["context_before"].apply(lambda x: " ".join(x)) + " <START> " + df["instance"].apply(lambda x: " ".join(x)) + " <END> " + df["context_after"].apply(lambda x: " ".join(x))
    X_df = []
    Y_df = []
    for index, rows in df.iterrows():
        X_df.append(rows.input)
        Y_df.append(rows.is_buggy)
        #print(rows.input)
    return X_df, Y_df
'''

#X_train = X_train[:1000000]
#Y_train = Y_train[:1000000]
#X_train = X_train[:1000000]
#Y_train = Y_train[:1000000]
X_train, Y_train = reshape_instances(train)
X_test, Y_test = reshape_instances(test)
X_valid, Y_valid = reshape_instances(valid)

#X_train = X_train[:510]
#Y_train = Y_train[:510]

# Build vocabulary and encoder from the training instances
vocabulary_set = set()
for data in X_train:
    vocabulary_set.update(data.split())

vocab_size = len(vocabulary_set)
print(vocab_size)

# Encode training, valid and test instances
encoder = tfds.features.text.TokenTextEncoder(vocabulary_set)

def encode(text):
  encoded_text = encoder.encode(text)
  return encoded_text

X_train = list(map(lambda x: encode(x), X_train))
X_test = list(map(lambda x: encode(x), X_test))
X_valid = list(map(lambda x: encode(x), X_valid))

X_train = pad_sequences(X_train, maxlen=maxlen)
X_test = pad_sequences(X_test, maxlen=maxlen)
X_valid = pad_sequences(X_valid, maxlen=maxlen)

# Model Definition
model = tf.keras.Sequential([
    tf.keras.layers.Embedding(encoder.vocab_size, 64),
    tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(64,  return_sequences=True)),
    tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(32)),
    tf.keras.layers.Dense(64, activation='relu'),
    tf.keras.layers.Dropout(0.5),
    tf.keras.layers.Dense(1, activation='sigmoid')
])

model.compile(loss='binary_crossentropy',
              optimizer=tf.keras.optimizers.Adam(1e-4),
              metrics=['accuracy'])

model.summary()
batch_size = 512

# Building generators
class CustomGenerator(Sequence):
    def __init__(self, text, labels, batch_size, num_steps=None):
        self.text, self.labels = text, labels
        self.batch_size = batch_size
        self.len = np.ceil(len(self.text) / float(self.batch_size)).astype(np.int64)
        if num_steps:
            self.len = min(num_steps, self.len)
    def __len__(self):
        return self.len
    def __getitem__(self, idx):
        batch_x = self.text[idx * self.batch_size:(idx + 1) * self.batch_size]
        batch_y = self.labels[idx * self.batch_size:(idx + 1) * self.batch_size]
        return batch_x, batch_y


train_gen = CustomGenerator(X_train, Y_train, batch_size)
valid_gen = CustomGenerator(X_valid, Y_valid, batch_size)
test_gen = CustomGenerator(X_test, Y_test, batch_size)




# Training the model
checkpointer = ModelCheckpoint('../../models/model-{epoch:02d}-{val_loss:.5f}.hdf5',
                               monitor='val_loss',
                               verbose=1,
                               save_best_only=True,
                               mode='min')

callback_list = [checkpointer] #, , reduce_lr
his1 = model.fit_generator(
                    generator=train_gen,
                    epochs=100,
                    validation_data=valid_gen,
                    callbacks=callback_list)
                    
                    
                    
                    
predIdxs = model.predict_generator(test_gen, verbose=1)

fpr, tpr, _ = roc_curve(Y_test, predIdxs)
roc_auc = auc(fpr, tpr)

plt.figure()
lw = 2
plt.plot(fpr, tpr, color='darkorange', lw=lw, label='ROC curve (area = %0.2f)' % roc_auc)
plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Receiver operating characteristic example')
plt.legend(loc="lower right")

plt.savefig('auc_model.png')
